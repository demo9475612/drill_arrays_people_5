function toMakeBalanceToNumber(datas){
      return datas.map(data => {
        let isNumber =(data.balance.slice(1)); 
        isNumber = isNumber.replace(',','');
        //console.log(isNumber);
        isNumber = Number((isNumber));
        if(isNumber){
         data.isNewBalance = parseFloat(data.balance.replace(/[^0-9.-]+/g,"")); 
        }else{
            data.isNewBalance = 0;
        }
        return data;
      });
}
module.exports = toMakeBalanceToNumber;