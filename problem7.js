const toMakeBalanceToNumber = require("./problem1");

function getTotalBalanceAvailable(datas){

       const newdatas = toMakeBalanceToNumber(datas);
       return newdatas.reduce((acc,data)=>{
          acc+=data.isNewBalance;
          return acc;
       },0);
}
module.exports= getTotalBalanceAvailable;