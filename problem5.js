function changeRegisteredDateFormat(datas){
             return datas.map(data =>{
                const currentDateTimeFormat = data.registered;
                const newDateTimeFormate = currentDateTimeFormat.substring(0,10).split('-');
                data.registered = `${newDateTimeFormate[2]}/${newDateTimeFormate[1]}/${newDateTimeFormate[0]}`
               // console.log(data);
               return data;

             });
}
module.exports = changeRegisteredDateFormat;