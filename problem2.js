function toSortDecendingOrderOfAge(datas){

       return datas.sort((people1,people2)=>{
           const people1Age=people1.age;
           const people2Age= people2.age;

           return people2Age-people1Age;
       });
}
module.exports = toSortDecendingOrderOfAge;